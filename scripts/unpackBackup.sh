#!/bin/bash

input="/tmp/"
output="/tmp"
run="0"
spill="0"
log="/tmp/${USER}"
prescale=1
begin_run=0
end_run=1000000

TEMP=`getopt -o i:o:r:s:l:p:b:e: --long input:,output:,prescale:,run:,spill:,log:,begin-run:,end-run: -n 'unpackBackup.sh' -- "$@"`
if [ $? != 0 ] ; then echo "Options are wrong..." >&2 ; exit 1 ; fi

eval set -- "$TEMP"

while true; do
case "$1" in
-i | --input ) input="$2"; shift 2 ;;
-o | --output ) output="$2"; shift 2 ;;
-l | --log ) log="$2"; shift 2 ;;
-r | --run ) run="$2"; shift 2;;
-s | --spill ) spill="$2"; shift 2;;
-p | --prescale ) prescale=$2; shift 2;;
-b | --begin-run ) begin_run=$2; shift 2;;
-e | --end-run ) end_run=$2; shift 2;;
-- ) shift; break ;;
* ) break ;;
esac
done

mkdir -p $log
if [[ "$spill" != "0" ]] && [[ "$run" != "0" ]]; then
    /home/cmsdaq/DAQ/H4DQM/bin/unpack -i $input  -o $output -r $run -s $spill  -p $prescale > $log/${run}_${spill}_unpack.log 2>&1
else
    for run in $(seq $begin_run $end_run)
    do
	cmd="ls ${input}/${run}"
	for file in `ssh pcethtb2 ${cmd}`
	do
	    spill=`echo ${file} | sed 's:.raw::g'`
	    if [[ ! -f "${output}/${run}/${spill}.root" ]]; then
		mkdir -p $input/$run/
		echo "Unpacking run ${run}, spill ${spill} ..."
		scp pcethtb2:$input/$run/$file $input/$run/
		/home/cmsdaq/DAQ/H4DQM/bin/unpack -i $input -o $output -r $run -s $spill -p $prescale > $log/${run}_${spill}_unpack.log 2>&1
	    fi
	    echo "raw/EB/$run/$spill.raw" > /var/spool/tbb/${run}_${spill}_raw
	    echo "raw/DataTree/$run/$spill.root" > /var/spool/tbb/${run}_${spill}_dataTree
	done
    done
fi

    
