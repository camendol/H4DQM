#include "interface/FITPIX.hpp"
#include "interface/Event.hpp"

int FITPIX::Unpack (dataType &stream, Event * event, boardHeader &bH) 
{
  WORD currWord = 0 ;
  adcData thisData;
  unsigned int eventSize_= bH.boardSize/4 -4;

  //First Event Header WORD
  stream.read ((char*)&currWord, WORDSIZE);  
  bool isSparseReadout = (currWord & 0x80000000)>>31;
  unsigned int  trigType= (currWord & 0x40000000)>>30;
  unsigned int  threshold=(currWord & 0x3FF00000)>>20;
  unsigned int  frame_duration=(currWord & 0xFFFFF);

  //Second Event Header WORD
  stream.read ((char*)&currWord, WORDSIZE);  
  unsigned int  tpxMode= (currWord & 0x80000000)>>31;
  unsigned int  clockFreq=currWord & 0x7FFFFFFF;

  //Trig time
  uint64_t  myTime;
  stream.read ((char*)&myTime, 2 * WORDSIZE) ;
  myTime*=10;
  timeData td; td.board=bH.boardID;
  td.time=myTime;
  event->evtTimes.push_back(td);

  stream.read ((char*)&myTime, 2 * WORDSIZE) ;
  myTime*=10;
  timeData td1; td1.board=bH.boardID;
  td1.time=myTime;
  event->evtTimes.push_back(td1);


  //Check that the eventSize matches
  stream.read ((char*)&currWord, WORDSIZE);  
  unsigned int payloadSize;
   if ( (currWord & 0xFFFF) == 0xFFFF)
     {
       //       isSparseReadout_=1;
       payloadSize=((currWord>>16)&0xFFFF)/2;
       if (payloadSize != eventSize_-6)
   	cout << "[FITPIX][Unpack]        | GOT WRONG NUMBER OF EVENT WORDS " 
   	 << eventSize_ << " EXPECTED " << payloadSize << "\n" ;
     }

   if (isSparseReadout)
     {
       for (unsigned int i=0;i<eventSize_-7;++i) 
   	{
   	  stream.read ((char*)&currWord, WORDSIZE);  
  	  
   	  thisData.board      =  bH.boardID; 
   	  thisData.channel    =  currWord & 0xFFFF;

          if (tpxMode != 0)
              thisData.adcReadout =  ((currWord>>16)&0xFFFF)*(1E6/clockFreq); //TOA, returning time in mus 
          else
              thisData.adcReadout =  ((currWord>>16)&0xFFFF); //TOT in clock units

     	  event->adcValues.push_back(thisData);
   	}
     }
   else
     {
       //full frame readout
       for (unsigned int i=0;i<eventSize_-7;++i) 
   	stream.read ((char*)&currWord, WORDSIZE);  
     }
  
  return(1);
}
