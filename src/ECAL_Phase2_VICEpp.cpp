/*  Data format
 *  -----------
 *   First 3 words: header, containing the timestamp
 *     information (i.e. the number of the 160 MHz clocks
 *     from the StartDAQ() command):
 *     timestamp = tMSBs<<32 + tLSB;
 *   Subsequent words: _nsamples for each of the 5 VFE channels
 *   Then concatenate header and samples for each VFE adapter
 *   read by the board.
 *
 *           32 ... 16 ...  0 -> bits
 *   words
 *           VFE_adapter_1
 *     0     [memory address] \
 *     1     [    tsLSBs    ]  |
 *     2     [    tsMSBs    ]  |-> header
 *     3     [   11111111   ]  |
 *     4     [   11111111   ]  |
 *     5     [   11111111   ] /
 *     6     [memory address] \
 *     7     []  |
 *     8     []  |
 *     9     []  |-> samples
 *     10    []  |
 *     11    []  |
 *     12    [memory address]  |
 *    ...       ...    ...    ...
 *           VFE_adapter_N
 *     0     [memory address] \
 *     1     [    tsMSBs    ]  |-> header
 *    ...       ...    ...    ...
 */

#include "interface/ECAL_Phase2_VICEpp.hpp"
#include <bitset>

int ECAL_Phase2_VICEpp::Unpack (dataType &stream, Event * event, boardHeader &bH)
{
    uint32_t header;
    stream.read ((char*)&header, sizeof(uint32_t));
    //unsigned int nSamples = (bH.boardSize - 4) /3 ; //move to hpp like dig1742Words_
    unsigned int nWords = headNSamples(header);
    unsigned int nDevices = headNDevices(header);
    unsigned int freq = headFrequency(header);
    
    size_t offset = event->digiValues.size();    
    event->digiValues.resize(offset + nDevices * nWords * 25,
                             {-1, 0, 0, 0, 0, 0, -1}); // 5 channels * 5 baseline words max

    uint32_t words[6]; 
    for (unsigned int idev = 0; idev < nDevices; ++idev) 
    {
        //---we do not want a variable event dataformat, fixing the size assuming the max number of true samples is n_words*5 (all baseline words)
        unsigned int ch_pos[5] = {0, 0, 0, 0, 0};
        unsigned int ch_offset[5];
        for (int ich = 0; ich < 5; ++ich) 
            ch_offset[ich] = offset + (idev * 5 + ich) * nWords * 5;

	uint32_t header[6];
	stream.read ((char*)header, 6 * sizeof(uint32_t));

        //---check for the "guard-ring" to be correct
        if(header[3]!=0xffffffff && header[4]!=0xffffffff && header[5]!=0xffffffff)
            std::cout << "[unpack][ECAL_Phase2_VICEpp] Warning: corrupted event header: " 
                      << header[3] << " "
                      << header[4] << " "
                      << header[5] << " " << std::endl;

	//---store number off clocks cycles since board init
	uint64_t ts = header[2];
        ts = (ts<<32) + header[1];
	timeData td{bH.boardSingleId, ts};
	event->evtTimes.push_back(td);

        for (unsigned int iWord = 0; iWord < nWords; ++iWord) {
            //---read next 6 words (i.e. one FPGA block)
            stream.read ((char*)words, 6 * sizeof(uint32_t));

            for (int ich = 0; ich < 5; ++ich) 
            {
                //---decompress DTU data
                int type = words[ich+1]>>30;
                int n_samples = type==2 ? std::min((words[ich+1]>>24)&0x3F, uint32_t(4)) : 5;

                //---compressed data
                if(type==1 || type==2)
                {
                    for(int is=0; is<n_samples; ++is)
                    {
                        auto idx = ch_pos[ich]+ch_offset[ich];

                        event->digiValues[idx].board = bH.boardSingleId; // FSM number
                        event->digiValues[idx].group = idev; // adapter number
                        event->digiValues[idx].channel = ich;
                        event->digiValues[idx].frequency = (freq+1)*40;
                        event->digiValues[idx].sampleIndex = ch_pos[ich];                        
                        event->digiValues[idx].sampleValue = words[ich+1] & 0x3F;
                        event->digiValues[idx].sampleGain = 1.;
                        ch_pos[ich]++;
                        words[ich+1] >>= 6;
                    }
                }
                //---uncompressed "signal" data
                else if(type != 3)
                {
                    int n_samples = ((words[ich+1]>>26)&0xF) == 0xA ? 2 : 1 ;
                    for(int is=0; is<n_samples; is++)
                    {
                        auto idx = ch_pos[ich]+ch_offset[ich];
                        event->digiValues[idx].board = bH.boardSingleId; // FSM number
                        event->digiValues[idx].group = idev; // adapter number
                        event->digiValues[idx].channel = ich;
                        event->digiValues[idx].frequency = (freq+1)*40;
                        event->digiValues[idx].sampleIndex = ch_pos[ich];
                        event->digiValues[idx].sampleValue = words[ich+1] & 0xFFF;
                        event->digiValues[idx].sampleGain = (words[ich+1]>>12)&0x1 == 1 ? 10. : 1.;
                        ch_pos[ich]++;
                        words[ich+1] >>= 13;                        
                    }
                }
            }
        }
    }
    return 0;
}
