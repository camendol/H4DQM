#ifndef FITPIX_H
#define FITPIX_H

#include "interface/Board.hpp"

#define FITPIX_SINGLE_CHIP_SIZE 65536
#define FITPIX_EVENT_SPARSEREADOUT_HEADER_WORDS 1

class FITPIX: public Board {

public:

  FITPIX() {} ;

  int Unpack (dataType &stream) { return -1 ; } ; //PG FIXME to be removed
  int Unpack (dataType &stream, Event * event, boardHeader &) ;

};

#endif
